<?php

namespace Config_Boletos\Abstract;

/**
 * Classe abstrata para a criação de pessoas dentro dos boletos
 * @package Boletos
 * @author Gabriel Souza
 */
abstract class PersonAbstract
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @var string
     */
    protected string $documentId;

    /**
     * @var string
     */
    protected string|null $address;

    /**
     * @var string
     */
    protected string|null $zipCode;

    /**
     * @var string
     */
    protected string|null $state;

    /**
     * @var string
     */
    protected string|null $city;

    public function __construct(
        string $name,
        string $documentId,
        string $address = null,
        string $zipCode = null,
        string $state = null,
        string $city = null
    ) {
        $scopeVars = get_defined_vars();
        foreach ($scopeVars as $varName => $value) {
            $this->genericSetter($varName, $value);
        }
    }

    private function genericSetter(string $varName, mixed $value): void
    {
        if (method_exists($this, 'set' . $varName)) {
            $this->{'set' . $varName}($value);

            return;
        }
        if (property_exists($this, $varName)) {
            $this->$varName = $value;
        }
    }

    private function setDocumentId(string $documentId): void
    {
        $clean = trim(preg_replace('/[^0-9]/', '', $documentId));
        $length = strlen($clean);

        match ($length) {
            11 => $this->documentId = $this->formatCpf($clean),
            14 => $this->documentId = $this->formatCnpj($clean),
            default => $this->documentId = $documentId
        };
    }

    private function setZipCode(string|null $zipCode): void
    {
        if (empty($zipCode)) {
            $this->zipCode = null;

            return;
        }

        $clean = trim(preg_replace('/[^0-9]/', '', $zipCode));
        $zipCode = substr($clean, 0, 5) . '-' . substr($clean, 5);
        $this->zipCode = $zipCode;

        return;
    }

    private function formatCpf(string $clean): string
    {
        return substr($clean, 0, 3) . '.' . substr($clean, 3, 3) . '.'
        . substr($clean, 6, 3) . '-' . substr($clean, 9);
    }

    private function formatCnpj(string $clean): string
    {
        return substr($clean, 0, 2) . '.' . substr($clean, 2, 3) .
        '.' . substr($clean, 5, 3) . '/' . substr($clean, 8, 4) . '-' .
        substr($clean, 12);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDocumentId(): string
    {
        return $this->documentId;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * Retorna uma string contendo o nome concatenado com o tipo de documento e o documento da pessoa.
     * Ex.: John Doe / CPF: 123.123.123-12
     *
     * @return string Os detalhes do nome e do documento da pessoa.
     */
    public function getNameAndDocument(): string
    {
        return $this->name . ' / ' . $this->getDocumentType() . ': ' . $this->documentId;
    }

    /**
     * Funçõa para descobrir qual o tipo de documento da pessoa, usando REGEX
     * Ex.:
     * * - 123.123.123.12 - CPF
     * * - 12.123.123/0001-12 - CNPJ
     * * Default: Documento
     * @return string O tipo de documento
     */
    public function getDocumentType(): string
    {
        $trimDocument = trim($this->documentId);

        if (preg_match('/^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})|([0-9]{11}))$/', $trimDocument)) {
            return 'CPF';
        } elseif (preg_match('/^(([0-9]{2}.[0-9]{3}.[0-9]{3}\/[0-9]{4}-[0-9]{2})|([0-9]{14}))$/', $trimDocument)) {
            return 'CNPJ';
        }

        return 'Documento';
    }

    /**
     * Retorna o endereço já formatado.
     *
     * Ex.: 10000-000 - Brasília - DF
     *
     * @return string O endereço formatado
     */
    public function getFormattedAddress(): string
    {
        $address = array_filter([$this->zipCode, $this->city, $this->state]);
        if (empty($address)) {
            return '';
        }

        return implode(' - ', $address);
    }
}
