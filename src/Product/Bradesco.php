<?php

namespace Config_Boletos\Product;

use Config_Boletos\Abstract\BoletoAbstract;
use Config_Boletos\Interface\BoletoInterface;

class Bradesco extends BoletoAbstract implements BoletoInterface
{
    protected string $bankCode = '237';
    protected string $bankLogo = 'bradesco.png';
    protected string $paymentLocation = 'Pagável em qualquer banco';
    protected array $portfolioAvailable = ['3', '4', '6', '9', '26'];
    protected string $htmlLayout = 'default.phtml';
    protected string $bankName = 'Bradesco';
    protected $cip = '000';
    /** Digito de auto conferencia do banco */
    protected string|null $selfCheckDigit = null;

    public function __construct(array $params = [])
    {
        parent::__construct($params);
        $this->setOurNumber();
    }
    public function setOurNumber(string|int $ourNumber = ''): void
    {
        if (!empty($ourNumber)) {
            /**Talvez */
            $this->ourNumber = substr($ourNumber, 0, -1);
            $this->selfCheckDigit = substr($ourNumber, -1);
            /**Fim do talvez */
            // $this->ourNumber = $ourNumber;

            return;
        }

        if (isset($this->ourNumber) && !empty($this->ourNumber)) {
            return;
        }

        $number = self::zeroFill($this->getSequential(), 11);
        $portfolio = self::zeroFill($this->getPortfolio(), 2);
        $digit = self::module11($portfolio . $number, 7);

        switch ($digit['rest']) {
            case 1:
                $this->selfCheckDigit = 'P';

                break;
            case 0:
                $this->selfCheckDigit = '0';

                break;
            default:
                $this->selfCheckDigit = $digit['digit'];
        }

        $this->ourNumber = $number;
    }

    public function getFreeField(): string
    {
        $step1 = self::zeroFill($this->getAgency(), 4);
        $step2 = self::zeroFill($this->getPortfolio(), 2);
        $step3 = self::zeroFill($this->getOurNumber(), 11);
        $step4 = self::zeroFill($this->getAccount(), 7);

        return $step1 . $step2 . $step3 . $step4 . '0';
    }

    public function setCip(string $cip): void
    {
        $this->cip = $cip;
    }

    public function getCip(): string
    {
        return $this->cip;
    }

    public function getSelfCheckDigit(): string|null
    {
        return $this->selfCheckDigit;
    }

    public function getViewVars(): array
    {
        $ournumber = str_pad($this->getPortfolio(), 2, '0', STR_PAD_LEFT) . '/' .
        str_pad($this->getOurNumber(), 11, '0', STR_PAD_LEFT);
        if (!empty($this->getSelfCheckDigit())) {
            $ournumber .= '-' . $this->getSelfCheckDigit();
        }
        $array = [
            'cip' => self::zeroFill($this->getCip(), 3),
            'show_cip' => true,
            'our_number' => $ournumber,
        ];

        return $array;
    }

    public function setSequential(int $sequential): void
    {
        $this->sequential = $sequential;
    }
}
