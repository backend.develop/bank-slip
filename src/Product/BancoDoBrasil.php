<?php

namespace Config_Boletos\Product;

use Config_Boletos\Abstract\BoletoAbstract;
use Config_Boletos\Exception\FinneBoletoException;
use Config_Boletos\Interface\BoletoInterface;

class BancoDoBrasil extends BoletoAbstract implements BoletoInterface
{
    protected string $bankCode = '001';
    protected string $bankLogo = 'bancodobrasil.jpg';
    protected string $paymentLocation = 'Pagável em qualquer banco';
    protected array $portfolioAvailable = ['31', '11', '16', '17', '18', '12', '51', '21'];
    protected string $htmlLayout = 'default.phtml';
    protected string $bankName = 'Banco do Brasil';


    public function __construct(array $params = [])
    {
        parent::__construct($params);
        $this->setOurNumber();
    }
    public function getAgreement(): string
    {
        if (!isset($this->agreement)) {
            throw new FinneBoletoException('Não foi encontrado o convênio');
        }
        if (!$this->agreement) {
            throw new FinneBoletoException('Não foi encontrado o convênio');
        }

        return $this->agreement;
    }

    public function setOurNumber(string|int $ourNumber = ''): void
    {
        if (!empty($ourNumber)) {
            $this->ourNumber = $ourNumber;

            return;
        }

        if (isset($this->ourNumber) && !empty($this->ourNumber)) {
            return;
        }

        $agreement = $this->getAgreement();
        $sequential = $this->getSequential();

        if (empty($sequential)) {
            throw new FinneBoletoException('Para gerar o nosso número, é necessário informar o sequencial.');
        }

        $ourNumber = $this->switchOurNumber($agreement, $sequential);
        $this->ourNumber = $ourNumber;

        return;
    }

    private function switchOurNumber(string $agreement, int $sequential): string
    {
        $lenghtAgreenment = strlen($agreement);

        switch ($lenghtAgreenment) {
            //Caso seja um conênio de 4 dígitos, o nosso número é de 11 dígitos
            case 4:
                $ourNumber = self::ZeroFill($agreement, 4) . self::ZeroFill($sequential, 7);

                break;
                //Caso o convenio seja de 5 dígitos, o nosso número é de 12 dígitos
                //No caso específico da carteiro 21, o nosso número é de 17 dígitos
            case 6:
                if ($this->portfolio == 21) {
                    $ourNumber = self::zeroFill($sequential, 17);
                } else {
                    $ourNumber = self::ZeroFill($agreement, 6) . self::ZeroFill($sequential, 6);
                }

                break;

                //Caso o convenio seja de 7 dígitos, o nosso número é de 17 dígitos
            case 7:
                $ourNumber = self::ZeroFill($agreement, 7) . self::ZeroFill($sequential, 10);

                break;
                //Caso não caia em nenhuma dessas condições, significa que está fora dos padrões do BB.
            default:
                throw new FinneBoletoException('Código de convênio não compatível com o Banco do Brasil');

                break;
        }

        $ourNumberLen = strlen($ourNumber);

        if ($ourNumberLen < 17) {
            $module = static::module11($ourNumber, 9);
            $ourNumber .= '-' . $module['digit'];
        }

        return $ourNumber;
    }

    protected function getFreeField(): string
    {
        $lenght = strlen($this->getAgreement());
        $ourNumber = $this->getOurNumber();
        $ourNumberLen = strlen($ourNumber);
        $sequentialLenght = strlen($this->getSequential() ?? 0);
        $portfolio = $this->getPortfolio();
        $agreement = $this->getAgreement();

        if ($ourNumberLen < 17) {
            $ourNumber = substr($ourNumber, 0, -2);
        }

        if ($sequentialLenght > 10) {
            if ($lenght == 6 and $portfolio == 21) {
                return self::zeroFill($agreement, 6) . $ourNumber . '21';
            }

            throw new FinneBoletoException('Para gerar um boleto de mais de 10 dígitos quando a' .
            ' carteira é 21 e o convênio possui 6 dígitos');
        }
        $freeField = '';
        switch ($lenght) {
            case 4:
            case 6:
                //Nesse caso, o nosso número é de 11 dígitos, a agencia 4, conta 8 e a carteira com 2 dígitos
                $freeField = $ourNumber . self::zeroFill($this->getAgency(), 4) .
                self::zeroFill($this->getAccount(), 8) . self::zeroFill($portfolio, 2);

                break;
            case 7:
                //Nesse caso, inicial de 6 zeros, com o nosso numero de 17 digitos e 2 digitos de carteira.
                $freeField = self::zeroFill('0', 6) . $ourNumber . self::zeroFill($portfolio, 2);

                break;
            default:
                throw new FinneBoletoException('Código de convênio não compatível com o Banco do Brasil');

                break;
        }

        return $freeField;
    }
}
