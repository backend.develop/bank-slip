<?php

namespace Config_Boletos\Examples;

use Boletos\Drawer;
use Boletos\Holder;

final class UY3BradescoExamples
{
    private static function createMock(): array
    {
        $holder = new Holder('LIBRA SECURITIZADORA S/A', '32493998000108 ', 'Rua Laguna, 242', '88301460', 'SC', 'Itajai');
        // $drawer = new Drawer('Nome de Cliente Fictício', '38542718004684', 'RUA JOANA D ARC - ANTA CRUZ', '36088390', 'MG', 'Juiz de Fora');
        $drawer = new Drawer('G & J INDUSTRIA E COMERCIO DE MARMORE SINTETICO E MARCENARIA LTDA', '02.072.716/0001-20', 'RUA JOANA D ARC - ANTA CRUZ', '36088390', 'MG', 'Juiz de Fora');

        return [
            'dateDue' => '2024-07-10',
            'dateDocument' => '2024-06-27',
            'datePayment' => '2024-06-27',
            'documentNumber' => '86986473',
            'quantity' => 1,
            'value' => 869.76,
            // 'sequential' => 12,
            'sequential' => '152699',
            'holder' => $holder,
            'drawer' => $drawer,
            'agency' => 0001,
            // 'agencyDigit' => 5,
            'account' => 8698647,
            'accountDigit' => 3,
            'portfolio' => '03',
            // 'agreement' => 3553013,
            'agreement' => 5053,
            'accept' => 'N',
            'instructions' => [ // Até 8
                'APÓS O VENCIMENTO COBRAR:',
                'MORA DIA/COM.PERMANENC  R$ 0,29 AO DIA',
                'MULTA DE 2,00% SOBRE O VALOR DO TÍTULO',
            ],
            // 'ourNumber' => '1234567',
        ];
    }

    public static function getMock(): array
    {
        $array = self::createMock();

        return $array;
    }
}
